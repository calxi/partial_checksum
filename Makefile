CC=sparc-ramp-gcc
CFLAGS=-O2 -std=c99 -g

all: make_chunk chk_chunk 

make_chunk: make_chunk.o

chk_chunk: chk_chunk.o

clean: 
	rm -rf *.o make_chunk chk_chunk
