#include<stdio.h>
#include<stdlib.h>
#include<time.h>


int check_512bytes(FILE *file, int index) {
   int *temp = malloc(512u); 
   int sum = 0;

   fseek(file, index * sizeof(int), SEEK_SET);
   fread(temp, 1, 512, file);

   for (int i = 0; i < 512/sizeof(int); i++)
   {
      if ( ((i + index) * (i + index) ^ (i + index)) != temp[i] ) 
      {
         free(temp);
         return 0;
      }
   }

   free(temp);

   return 1;

}

int main(int argc, char *argv[]) {

   if (argc != 4)
   {
      printf("usage: %s path-to-test-file size num_iter \n", argv[0]);
      return 1;
   }

   FILE *f;
   f = fopen(argv[1], "rb");

   srand(time(NULL));
   int num_iter = atoi(argv[3]), size = atoi(argv[2]);

   for (int i = 0; i < num_iter; i++)
   {
      /* generate a random index within the max size, and verify that block. */
      int index = rand() % size; 
      if (check_512bytes(f, index * 512) == 0)
      {
         printf("Checksum error at block: %i \n", index);
         fclose(f);
         return 1;
      }
   }
   fclose(f);

   return 0;
}
