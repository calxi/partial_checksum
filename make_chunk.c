#include<stdio.h>
#include<stdlib.h>


int main(int argc, char *argv[]) {

   if (argc != 3)
   {
      printf("usage %s path-to-file size\n", argv[0]);
      return 1;
   }

   FILE *f;
   f = fopen(argv[1], "wb");

   int size = atoi(argv[2]);

   for (int i = 0; i < 512 * size; i++) //, fseek(f, sizeof(int), SEEK_CUR))
   {
      int temp = (i * i) ^ i;
      fwrite(&temp, sizeof(int), 1, f);
   }
   fclose(f);

   return 0;
}
